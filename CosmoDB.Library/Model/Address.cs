﻿namespace CosmoDB.Library.Model
{
    public class Address
    {
        public int addressId { get; set; }
        public int addressTypeId { get; set; }
        public string street1 { get; set; }
        public string street2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
    }
}