﻿using System;

namespace CosmoDB.Library.Model
{
    public class Invoice
    {
        public int invoiceId { get; set; }
        public DateTime date { get; set; }
        public decimal amount { get; set; }
        public DateTime? paidDate { get; set; }
        public decimal paidAmount { get; set; }
    }
}