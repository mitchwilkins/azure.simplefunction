﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CosmoDB.Library.Model;
using Microsoft.Azure.Cosmos;
using Newtonsoft.Json;

namespace CosmoDB.Library.Classes
{
    public class SQL
    {
        private readonly string _clientString = "";
        public string Db { get; set; }
        public string Table { get; set; }

        public SQL(string clientString, string dbName)
        {
            _clientString = clientString;
            Db = dbName;
        }

        public SQL(string clientString, string dbName, string tableName) : this(clientString, dbName)
        {
            Table = tableName;
        }

        public string GetData<T>()
        {
            using (CosmosClient client = new CosmosClient(_clientString))
            {
                Database db = client.GetDatabase(Db);
                Container table = client.GetContainer(db.Id, Table);

                string sqlString = $"SELECT * FROM t";
                FeedIterator<T> iterator = table.GetItemQueryIterator<T>(sqlString);

                FeedResponse<T> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                T customer = queryResult.FirstOrDefault();

                return JsonConvert.SerializeObject(customer);
            };
        }
        
        public string GetData<T>(string sqlString)
        {
            using (CosmosClient client = new CosmosClient(_clientString))
            {
                Database db = client.GetDatabase(Db);
                Container table = client.GetContainer(db.Id, Table);


                FeedIterator<T> iterator = table.GetItemQueryIterator<T>(sqlString);

                FeedResponse<T> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

                T customer = queryResult.FirstOrDefault();

                return JsonConvert.SerializeObject(customer);
            };
        }
        
        // public string PutData<T>(T model) where T : new()
        // {
        //     using (CosmosClient client = new CosmosClient(_clientString))
        //     {
        //         Database db = client.GetDatabase(Db);
        //         Container table = client.GetContainer(db.Id, Table);
        //
        //
        //         FeedIterator<T> iterator = table.GetItemQueryIterator<T>(model);
        //
        //         FeedResponse<T> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();
        //
        //         T customer = queryResult.FirstOrDefault();
        //
        //         return JsonConvert.SerializeObject(customer);
        //     };
        // }
    }
}