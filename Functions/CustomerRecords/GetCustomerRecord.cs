﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using CosmoDB.Library.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Azure.SimpleFunction.Functions.CustomerRecords
{
    public static class GetCustomerRecord
    {
        [FunctionName("GetCustomerRecord")]
        public static async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]
            HttpRequest req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string id = req.Query["id"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            id ??= data?.id;

            string responseMessage = "";
            
            if (string.IsNullOrEmpty(id))
            {
                responseMessage = "Invalid Id.";
            }
            else
            {

                try
                {
                    responseMessage = CustomerRecords.Sql.GetData<Customer>($"SELECT * FROM t where t.customerId = {id}");
                }
                catch (Exception ex)
                {
                    return new BadRequestObjectResult(ex.Message);
                }

            }
            return new OkObjectResult(responseMessage);
        }
    }
}