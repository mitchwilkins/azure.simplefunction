﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CosmoDB.Library.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Azure.SimpleFunction.Functions.CustomerRecords
{

        public class GetCustomerRecords
        {

            [FunctionName("GetCustomerRecords")]
            public static async Task<IActionResult> RunAsync(
                [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)]
                HttpRequest req, ILogger log)
            {
                log.LogInformation("C# HTTP trigger function processed a request.");

                string responseMessage = "";

                try
                {
                    responseMessage = CustomerRecords.Sql.GetData<Customer>();
                }
                catch (Exception ex)
                {
                    return new BadRequestObjectResult(ex.Message);
                }

                return new OkObjectResult(responseMessage);
            }
        }

}