﻿using System;
using System.IO;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using CosmoDB.Library.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Azure.SimpleFunction.Functions.CustomerRecords
{
    public static class PostCustomerRecords
    {
        [FunctionName("PostCustomerRecords")]
        public static async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]
            Customer req, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            if (req.GetType() != typeof(Customer))
                return new BadRequestObjectResult("Bad model state.");

            string customer = JsonConvert.ToString(req);

            return new OkObjectResult("");
        }
    }
}